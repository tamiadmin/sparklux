var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Order = new mongoose.Schema({
    name: String,
    phone: String,
    flowers: [],
    address: String,
    price: Number
});

module.exports = mongoose.model('Order', Order);
