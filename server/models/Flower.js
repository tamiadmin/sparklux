var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Flower = new mongoose.Schema({
  name: String,
  desc: String,
  image: String,
  event: String,
  price: Number,
});

module.exports = mongoose.model('Flower', Flower);
