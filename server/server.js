const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const passport = require('passport');
const nodemailer = require('nodemailer');
var mongoose = require('mongoose');
var multer = require('multer');
mongoose.connect('mongodb://stas:stas12345@ds027495.mlab.com:27495/remont');
var upload = multer({ dest: './client/public/images/' });
var Order = require('./models/Order.js');
var Flower = require('./models/Flower.js');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'takhirmunarbekov@gmail.com',
    pass: 'tuinpylstgcpfllj',
  },
});

const app = express();
const port = process.env.PORT || 3002;

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(
  require('prerender-node').set('prerenderToken', '0ZMJRJNi2tTlvdro6Cyz'),
);

app.use(passport.initialize());

// require('./config/passport')(passport);

app.use(express.static('client/public/'));
app.use('/images/', express.static('../client/images'));
app.use('/fonts/', express.static('../client/style/fonts'));

app.post('/api/order', function(req, res, next) {
  let post = new Order({
    name: req.body.name,
    phone: req.body.phone,
    flowers: req.body.cart,
    price: req.body.price,
    address: req.body.address,
  });

  post.save(function(err) {
    if (err) return res.status(400).end();
    res.send(post);
  });
});



app.post('/api/flowers', function(req, res, next) {
  for (let index = 0; index < req.body.length; index++) {
    let post = new Flower({
      name: req.body.name,
      desc: req.body.desc,
      event: req.body.event,
      price: req.body.price,
      image: req.body.image,
    });

    post.save(function(err) {
      console.log(err);
      if (err) return res.status(400).end();
      res.send(post);
    });
  }
});

app.get('/api/flowers', function(req, res, next) {
  Flower.find({}).exec(function(err, posts) {
    if (err) return res.status(400).end();
    res.send(posts);
  });
});

app.get('/api/orders', function(req, res, next) {
  Order.find({}).exec(function(err, posts) {
    if (err) return res.status(400).end();
    res.send(posts);
  });
});

// app.post('/sendmail/', function(req, res) {
//   console.log(req.body.mail);

//   const msg = {
//     to: 'takhirmunarbekov@gmail.com',
//     from: req.body.mail,
//     subject: req.body.name + ' (Проект с сайта)',
//     text: req.body.msg + ' | ' + req.body.mail,
//   };

//   transporter.sendMail(msg, function(error, info) {
//     if (error) {
//       console.log(error);
//     } else {
//       console.log('Email sent: ' + info.response);
//     }
//   });

//   res.send(true);
// });

app.get('*', (req, res) => {
  res.sendFile('index.html', {
    root: path.join(__dirname, '../client/public/'),
  });
});

app.listen(port, () => {
  console.log(`SERVER RUNNNING ON PORT ${port}`);
});

module.exports = app;
