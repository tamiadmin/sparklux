import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
// import SVG from 'react-inlinesvg';

import './header.scss';
import axios from 'axios';
import { connect } from 'react-redux';
import { addCart, processBuy } from '../../reducers/clientReducer';

const isNumericInput = event => {
  const key = event.keyCode;
  return (
    (key >= 48 && key <= 57) || // Allow number line
    (key >= 96 && key <= 105) // Allow number pad
  );
};

const isModifierKey = event => {
  const key = event.keyCode;
  return (
    event.shiftKey === true ||
    key === 35 ||
    key === 36 || // Allow Shift, Home, End
    key === 8 || key === 9 || key === 13 || key === 46 || // Allow Backspace, Tab, Enter, Delete
    (key > 36 && key < 41) || // Allow left, up, right, down
    // Allow Ctrl/Command + A,C,V,X,Z
    ((event.ctrlKey === true || event.metaKey === true) &&
      (key === 65 || key === 67 || key === 86 || key === 88 || key === 90))
  );
};

const enforceFormat = event => {
  // Input must be of a valid number format or a modifier key, and not longer than ten digits
  if (!isNumericInput(event) && !isModifierKey(event)) {
    event.preventDefault();
  }
};

const formatToPhone = event => {
  if (isModifierKey(event)) {
    return;
  }

  // I am lazy and don't like to type things more than once
  const target = event.target;
  const input = target.value.replace(/\D/g, '').substring(1, 11); // First ten digits of input only
  const zip = input.substring(0, 3);
  const middle = input.substring(3, 6);
  const last = input.substring(6, 11);

  if (input.length > 6) {
    return `+7(${zip}) ${middle} - ${last}`;
  } else if (input.length > 3) {
    return `+7(${zip}) ${middle}`;
  } else if (input.length > 0) {
    return `+7(${zip}`;
  }
};

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bgWidth: '',
      isTop: true,
      showForm: false,
      name: '',
      phone: '',
      address: '',
    };
    this.handleScroll = this.handleScroll.bind(this);
    this.changeForm = this.changeForm.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  changeForm(e, type) {
    this.setState({
      [type]: e.target.value,
    });
  }

  changePhone(e) {
    const value = formatToPhone(e);
    this.setState({
      phone: value,
    });
  }

  processBuy() {
    if (this.state.name !== '' && this.state.phone !== '') {
      this.props.processBuy(
        this.props.client.cart,
        this.props.client.price,
        this.state.name,
        this.state.phone,
        this.state.address,
      );
    }
  }

  handleScroll(e) {
    const isTop = pageYOffset < 100 || false;
    this.setState({
      isTop: isTop,
    });
  }

  openForm() {
    this.setState({
      showForm: true,
    });
  }

  render() {
    return (
      <Fragment>
        <div className={`header ${this.state.isTop ? null : 'header-top'}`}>
          <div className={'header-bg ' + `header-bg-${this.state.bgWidth}`}>
            <div className='header-bg-img'>
              <img src='../images/back/bg.svg' />
            </div>
          </div>
          <div className='header-insta'>
            <a
              href='https://api.whatsapp.com/send?phone=77075090672&text=%D0%97%D0%B4%D1%80%D0%B0%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5'
              target='_blank'
            >
              <img src='../images/insta.svg' alt='' />
            </a>
          </div>
          <div className='container'>
            <div className='header-inner'>
              <div className='logo'>
                <img src='../images/logo.png' alt='' />
              </div>
              <div className='menu'>
                <div className='menu-i'>
                  <span>г. Алматы ул. Гоголя 86 офис 219</span>
                </div>
                <div className='menu-i'>
                  <a href='tel:+77075090672'>+7 707 509 0672</a>
                </div>
              </div>
              <div className='header-contact'>
                <span uk-toggle='target: #offcanvas-flip'>
                  Корзина ( {this.props.client.cart.length} )
                  <img src='../images/icons/basket.svg' alt='' />
                </span>
                <div
                  id='offcanvas-flip'
                  uk-offcanvas='flip: true; overlay: true'
                >
                  <div className='uk-offcanvas-bar header-modal'>
                    <div className='header-modal-inner'>
                      <h3 className='header-modal-title'>Корзина</h3>
                      {/* <h3 className='header-modal-sum'>
                        Сумма : {this.props.client.price} тнг
                      </h3> */}
                      {this.props.client.proceed ? (
                        <div className={'header-modal-items-thanks'}>
                          Спасибо за заказ
                        </div>
                      ) : !this.state.showForm ? (
                        this.props.client.cart.length < 1 ? (
                          <div className={'header-modal-items-empty'}>
                            Корзина пуста
                          </div>
                        ) : (
                          <div className='header-modal-items'>
                            {this.props.client.cart.map((i, k) => {
                              return (
                                <div className='header-modal-items-i'>
                                  <div
                                    className='header-modal-items-i-img'
                                    style={{
                                      backgroundImage: `url(${i.image})`,
                                    }}
                                  >
                                    {/*<img src={i.image} alt=""/>*/}
                                  </div>
                                  <div className='header-modal-items-i-inner'>
                                    <div className='header-modal-items-i-name'>
                                      {i.name}
                                    </div>
                                    <div className='header-modal-items-i-price'>
                                      {i.price} шт
                                    </div>
                                  </div>
                                </div>
                              );
                            })}
                          </div>
                        )
                      ) : (
                        <div className='header-modal-form'>
                          <input
                            type='text'
                            placeholder={'Имя'}
                            value={this.state.name}
                            onChange={e => this.changeForm(e, 'name')}
                          />
                          {/*<PhoneInput defaultCountry={'kz'} placeholder={'Номер телефона'}*/}
                          {/*            value={this.state.phone}*/}
                          {/*            onChange={(e) => this.changePhone(e)}/>*/}

                          <input
                            type='text'
                            placeholder={'Номер телефона'}
                            value={this.state.phone}
                            onChange={e => this.changePhone(e, 'phone')}
                          />

                          <input
                            type='text'
                            placeholder={'Адрес доставки'}
                            value={this.state.address}
                            onChange={e => this.changeForm(e, 'address')}
                          />
                        </div>
                      )}
                    </div>
                    <div className='header-modal-button'>
                      {this.state.showForm ? (
                        this.props.client.proceed ? null : this.state.phone !==
                            '' &&
                          this.state.name !== '' &&
                          this.state.address ? (
                          <span onClick={() => this.processBuy()}>
                            Завершить заказ
                          </span>
                        ) : null
                      ) : this.props.client.cart.length > 0 ? (
                        <span onClick={() => this.openForm()}>
                          Оформить заказ
                        </span>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  client: state.client,
});

export default connect(mapStateToProps, {
  addCart,
  processBuy,
})(Header);
