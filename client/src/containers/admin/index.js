import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import './admin.scss';
import { Carousel } from 'antd';
import 'antd/lib/carousel/style/index.css';
import { connect } from 'react-redux';
import { addCart, processBuy } from '../../reducers/clientReducer';
import axios from 'axios';

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      pass: '',
      log: true,
      orders: [],
      order: null,
      flowers: [],
    };
  }

  componentDidMount() {
    axios
      .get(`/api/orders/`)
      .then(response => {
        console.log(response.data);
        this.setState({
          orders: response.data,
        });
      })
      .catch(err => {});

    axios
      .get(`/api/flowers/`)
      .then(response => {
        console.log(response.data);
        this.setState({
          flowers: response.data,
        });
      })
      .catch(err => {});
  }

  logIn() {
    if (this.state.name === 'stas' && this.state.pass === 'stas') {
      this.setState({
        log: true,
      });
    }
  }

  changeForm(e, f) {
    this.setState({
      [f]: e.target.value,
    });
  }

  selectOrder(i) {
    this.setState({
      order: i,
    });
  }

  render() {
    return (
      <Fragment>
        <div className='a'>
          <div className='a__inn'>
            <div className='container'>
              <div className='a__inn-title'>Админ</div>
              {this.state.log === false ? (
                <div className='a__inn-form'>
                  <div className='a__inn-form-label'>Логин</div>
                  <input
                    type='text'
                    placeholder=''
                    value={this.state.name}
                    onChange={e => this.changeForm(e, 'name')}
                  />
                  <div className='a__inn-form-label'>Пароль</div>
                  <input
                    type='password'
                    value={this.state.pass}
                    onChange={e => this.changeForm(e, 'pass')}
                  />
                  <div
                    className='a__inn-form-button'
                    onClick={() => this.logIn()}
                  >
                    Войти
                  </div>
                </div>
              ) : (
                <div className='a__inn-admin'>
                  <div className='a__inn-flowers'>
                    <div className='a__inn-flowers-items'>
                      <div className='a__inn-flowers-title'>Заказы</div>
                      <div className='a__inn-flowers-content'>
                        {this.state.orders.map((i, k) => {
                          return (
                            <div
                              className='a__inn-flowers-items-i'
                              onClick={() => this.selectOrder(i)}
                            >
                              <div className='a__inn-flowers-items-i-title'>
                                {i.name}
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                    <div className='a__inn-flowers-item'>
                      <div className='a__inn-flowers-title'>Заказ</div>
                      {this.state.order === null ? (
                        <div className={'a__inn-flowers-item-none'}>
                          Выберите заказ
                        </div>
                      ) : (
                        <div className='a__inn-flowers-item-inner'>
                          <div className='a__inn-flowers-item-name'>
                            Имя: {this.state.order.name}
                          </div>
                          <div className='a__inn-flowers-item-name'>
                            Телефон: {this.state.order.phone}
                          </div>
                          {/* <div className='a__inn-flowers-item-name'>
                            шт: {this.state.order.price}
                          </div> */}
                          <div className='a__inn-flowers-item-name'>
                            Ардес: {this.state.order.address}
                          </div>
                          <div className='a__inn-flowers-item-orders'>
                            <div className='a__inn-flowers-item-orders-title'>
                              Товары:
                            </div>
                            {this.state.order.flowers.map((i, k) => {
                              return (
                                <div
                                  className='a__inn-flowers-item-orders-i'
                                  key={k}
                                >
                                  <div
                                    className='a__inn-flowers-item-orders-i-img'
                                    style={{
                                      backgroundImage: `url(${i.image})`,
                                    }}
                                  ></div>
                                  <div className='a__inn-flowers-item-orders-i-inner'>
                                    <div className='a__inn-flowers-item-orders-i-name'>
                                      {i.name}
                                    </div>
                                    <div className='a__inn-flowers-item-orders-i-name'>
                                      {i.address}
                                    </div>
                                    <div className='a__inn-flowers-item-orders-i-price'>
                                      {i.price} шт
                                    </div>
                                  </div>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  client: state.client,
});

export default connect(mapStateToProps, {
  addCart,
  processBuy,
})(Admin);
