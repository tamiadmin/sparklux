import React, { Fragment, Component, useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';
import './landing.scss';
import { Carousel } from 'antd';
import 'antd/lib/carousel/style/index.css';
import axios from 'axios';
import { connect } from 'react-redux';
import { YMaps, Map, Placemark } from 'react-yandex-maps';

import { addCart } from '../../reducers/clientReducer';


const mapData = {
  center: [43.260439, 76.933999],
  zoom: 17,
};

const coordinates = [[43.260439, 76.933999]];

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flowers: [],
      slice: 6,
      more: true,
    };
  }

  componentDidMount() {
    axios
      .get(`/api/flowers/`)
      .then(response => {
        this.setState({
          flowers: response.data,
        });
      })
      .catch(err => {});
  }

  sliceOpen() {
    this.setState({
      slice: this.state.flowers.lendth,
      more: false,
    });
  }

  addToCart(i) {
    this.props.addCart(i);
  }

  sliceClose() {
    this.setState({
      slice: 6,
      more: true,
    });
  }

  render() {
    console.log(this.props);

    return (
      <Fragment>
        <div className='l'>
          <div className='l__inn'>
            <div className='l__inn-welcome'>
              <Carousel autoplay dots={false} effect='fade'>
                <div className='l__inn-welcome-i l__inn-welcome-i-1'>
                  <div className='container'>
                    <div className='l__inn-welcome-i-inner'>
                      <span className='l__inn-welcome-i-inner-text'>
                        <h5>Низкие цены крупным поставщикам</h5>
                      </span>
                    </div>
                  </div>
                </div>
                <div className='l__inn-welcome-i l__inn-welcome-i-2'>
                  <div className='container'>
                    <div className='l__inn-welcome-i-inner'>
                      <span className='l__inn-welcome-i-inner-text'>
                        <h5>Бесплатная доставка по городу Алматы</h5>
                      </span>
                    </div>
                  </div>
                </div>
                <div className='l__inn-welcome-i l__inn-welcome-i-3'>
                  <div className='container'>
                    <div className='l__inn-welcome-i-inner'>
                      <span className='l__inn-welcome-i-inner-text'>
                        <h5>Индивидуальный подход к каждому клиенту</h5>
                      </span>
                    </div>
                  </div>
                </div>
              </Carousel>
            </div>

            <div className='l__inn-services'>
              <div className='container'>
                <div className='l__inn-services-items'>
                  <div className='l__inn-services-items-i'>
                    <div className='l__inn-services-items-i-img'>
                      <img src='../images/icons/bouqet.svg' alt='' />
                    </div>
                    <div className='l__inn-services-items-i-text'>
                      Одни из лучших по качеству
                    </div>
                  </div>
                  <div className='l__inn-services-items-i'>
                    <div className='l__inn-services-items-i-img'>
                      <img src='../images/icons/stopwatch.svg' alt='' />
                    </div>
                    <div className='l__inn-services-items-i-text'>
                      Склад и офис находятся в одном месте
                    </div>
                  </div>
                  <div className='l__inn-services-items-i'>
                    <div className='l__inn-services-items-i-img'>
                      <img src='../images/icons/valentines.svg' alt='' />
                    </div>
                    <div className='l__inn-services-items-i-text'>
                      Бесплатная доставка по городу Алматы
                    </div>
                  </div>
                  <div className='l__inn-services-items-i'>
                    <div className='l__inn-services-items-i-img'>
                      <img src='../images/icons/tux.svg' alt='' />
                    </div>
                    <div className='l__inn-services-items-i-text'>
                      Более сотни видов товаров для любых случаев
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className='l__inn-catalog'>
              <div className='container'>
                <div className='l__inn-catalog-content'>
                  <h1 className='l__inn-catalog__text'>
                    Наши товары
                  </h1>
                  {/* <div className='l__inn-catalog__title'>
                    <span>Новинки</span>
                    <span>
                      По повадам <br /> скоро!
                    </span>
                  </div> */}
                  <div className='l__inn-catalog__items'>
                    {this.state.flowers
                      .slice(0, this.state.slice)
                      .map((i, k) => {
                        return (
                          <div className='l__inn-catalog__items-i' key={k}>
                            <div
                              className='l__inn-catalog__items-i-img'
                              style={{ backgroundImage: `url(${i.image})` }}
                            ></div>
                            <div className='l__inn-catalog__items-i-inner'>
                              <div className='l__inn-catalog__items-i-title'>
                                {i.name}
                              </div>
                              <div className='l__inn-catalog__items-i-about'>
                                {i.desc}
                              </div>
                              <div className='l__inn-catalog__items-i-price'>
                                <div className='l__inn-catalog__items-i-price-count'>
                                  {i.price} шт
                                </div>
                                {this.props.client.cart.find(
                                  obj => obj.name === i.name,
                                ) ? (
                                  <div className='l__inn-catalog__items-i-price-cart l__inn-catalog__items-i-price-cart-active'>
                                    В корзинe
                                  </div>
                                ) : (
                                  <div
                                    className='l__inn-catalog__items-i-price-cart '
                                    onClick={() => this.addToCart(i)}
                                  >
                                    В корзину
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                  </div>
                  <div className='l__inn-catalog__more'>
                    {this.state.more ? (
                      <span onClick={() => this.sliceOpen()}>Показать еще</span>
                    ) : (
                      <span onClick={() => this.sliceClose()}>Cкрыть</span>
                    )}
                  </div>
                </div>
              </div>
            </div>

            {/* <img className='l__inn-div' src='../images/div.png' alt='' /> */}

            <div className='l__inn-about'>
              <div className='container'>
                <div className='l__inn-about-content'>
                  <div className='l__inn-about-inner'>
                    <div className='l__inn-about-text'>
                      <div className='l__inn-about-title'>
                        <span>О нас</span>
                      </div>
                      Spark Lux молодая, прогрессивная, быстро развевающаяся
                      компания на рынке строительных инструментов. Наша компания
                      занимаемся оптовой и мелкооптовой торговлей таких
                      направлений как: Ручные инструменты, Садовая техника и
                      инструменты, Хозяйственные товары и Электрика. Приоритет
                      нашей компании рынок СНГ.
                      <br /> Мы уважаем и трепетно относимся к своих клиентов и
                      даём им выгодные цены на весь наш ассортимент. Наши
                      приемущества: Бесплатная доставка по городу Алматы Низкие
                      цены крупным поставщикам Одни из лучших по качеству
                      Индивидуальный подход к каждому клиенту Склад и офис
                      находятся в одном месте
                    </div>
                    <div className='l__inn-about-img'>
                      <img src='' alt='' />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='l__inn-ind'>
              <div className='container'>
                <div className='l__inn-ind-content'>
                  <div className='l__inn-ind-img'>
                    <img src='/images/back/ind.jpg' alt='' />
                  </div>
                  <div className='l__inn-ind-text'>
                    <h1>Не нашли подходящий товар?</h1>
                    <h3>
                      Для просмотра полного ассортимента товаров скачайте .
                      <br /> Наш Каталог ниже.
                    </h3>
                    <div className='l__inn-ind-text-button'>
                      <a
                        href='https://api.whatsapp.com/send?phone=77075090672&text=%D0%97%D0%B4%D1%80%D0%B0%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5,%20%D0%AF%20%D1%85%D0%BE%D1%82%D0%B5%D0%BB%20%D0%B1%D1%8B%20%D0%BF%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D1%8C%20%D0%BF%D0%BE%D0%BB%D0%BD%D1%8B%D0%B9%20%D0%B0%D1%81%D1%81%D0%BE%D1%80%D1%82%D0%B8%D0%BC%D0%B5%D0%BD%D1%82%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2'
                        target='_blank'
                      >
                        Скачать каталог
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* <img className='l__inn-div' src='../images/div.png' alt='' /> */}

            <div className='l__inn-footer'>
              <div className='container'>
                <div className='l__inn-footer-content'>
                  <div className='l__inn-footer-map'>
                    <YMaps className='l__inn-footer-map-inn'>
                      <Map defaultState={mapData} width={'100%'} height={320}>
                        {coordinates.map(coordinate => (
                          <Placemark
                            geometry={coordinate}
                            options={{
                              iconColor: '#00c4cc',
                              presetStorage: 'islands#circleIcon',
                            }}
                          />
                        ))}
                      </Map>
                    </YMaps>
                  </div>
                  <div className='l__inn-footer-contacts'>
                    <div className='l__inn-footer-contacts-address'>
                      <h3>Адрес:</h3>
                      <p>г. Алматы ул. Гоголя 86 офис 219</p>
                    </div>
                    <div className='l__inn-footer-contacts-address'>
                      <h3>Почта:</h3>
                      <p>

                      <a href="mailto:so-sklada-spark_lux@mail.ru">so-sklada-spark_lux@mail.ru</a>
                        </p>
                    </div>
                    <div className='l__inn-footer-contacts-row'>
                      <div className='l__inn-footer-contacts-row-i'>
                        <h3>Время работы:</h3>
                        <p>6:00 - 19:00 Вт-Вс</p>
                      </div>
                    </div>
                    <div className='l__inn-footer-contacts-row'>
                      <div className='l__inn-footer-contacts-row-i'>
                        <h3>Телефоны :</h3>
                        <p>
                          <a href="tel:+77075090672">+7 707 509 0672</a>
                          
                          </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  client: state.client,
});

export default connect(mapStateToProps, {
  addCart,
})(Main);
